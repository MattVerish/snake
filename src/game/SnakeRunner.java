package game;

public class SnakeRunner {

	public static void main(String[] args) {
		SnakeGui Gui = new SnakeGui();
		
		while (!Gui.gameOver()) {
			try {
				Thread.sleep(300);
				Gui.step();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Game Over");
	}
}
