package game;

public class Triplet {
	private int X;
	private int Y;
	private int Dir;
	
	public Triplet(int X, int Y, int Dir) {
		this.X = X;
		this.Y = Y;
		this.Dir = Dir;
	}
	
	public Triplet(Triplet another) {
		int[] info = another.getInfo();
		this.X = info[0];
		this.Y = info[1];
		this.Dir = info[2];
	}
	
	public int[] getInfo() {
		int[] info = new int[] {this.X, this.Y, this.Dir};
		return info;
	}
	
	public void updatePosition(int newDir) {
		if (this.Dir == 0) {
			this.Y = this.Y - 1;
		} else if (this.Dir == 1) {
			this.X = this.X + 1;
		} else if (this.Dir == 2) {
			this.Y = this.Y + 1;
		} else {
			this.X = this.X - 1;
		}
		this.Dir = newDir;
	}
	
	
	
	public boolean equals(Triplet t) {
		int[] tInfo = t.getInfo();
		int[] myInfo = this.getInfo();
		
		for (int i = 0; i < 3; i++) {
			if (tInfo[i] != myInfo[i]) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isLocEqual(Triplet t) {
		int[] tInfo = t.getInfo();
		int[] myInfo = this.getInfo();
		
		if (tInfo[0] == myInfo[0] && tInfo[1] == myInfo[1]) {
			return true;
		}
		return false;
	}
	
	public int getDir() {
		return Dir;
	}
	
	public void setDir(int newDir) {
		Dir = newDir;
	}
}
