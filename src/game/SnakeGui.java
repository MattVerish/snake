package game;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.*;

public class SnakeGui extends JFrame implements KeyListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton buttonReset;
	private JLabel score;
	private int Score = 0;
	private Snake snake;
	private MyPanel panel;
	private static final int boardSize = 10;
	private int[] foodLoc = new int[2];
	private boolean gameOver = false;
	
	public SnakeGui() {
		createView();
		
		setTitle("Snake");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setSize(600, 600);
		setLocationRelativeTo(null);
		setVisible(true);
		
		snake = new Snake(10);
	}
	
	public boolean gameOver() {
		return gameOver;
	}
	
	public void step() {
		Score -= 1;
		snake.updateSnake();
		if (snake.checkCollision()) {
			gameOver = true;
			return;
		}
		int[] headInfo = snake.getHeadInfo();
		if (headInfo[0] == foodLoc[0] && headInfo[1] == foodLoc[1]) {
			Score += 100;
			moveFood();
			snake.growSnake();
		}
		score.setText("Score: " + Score);
		panel.repaint();
	}
	
	public void moveFood() {
		int foodX, foodY;
		do {
			Random generator = new Random();
			foodX = generator.nextInt(boardSize);
			foodY = generator.nextInt(boardSize);
		} while(snake.isOccupied(foodX, foodY));
		
		foodLoc[0] = foodX;
		foodLoc[1] = foodY;
		
		panel.repaint();
	}
	
	public void createView() {
		panel = new MyPanel();
		
		
		buttonReset = new JButton("Reset");
		buttonReset.setFocusable(true);
		buttonReset.addKeyListener(this);
		panel.add(buttonReset);
		
		score = new JLabel("Score: " + Score);
		score.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(score);
		
		panel.setBackground(Color.GREEN);
		getContentPane().add(panel);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SnakeGui();
			}
		});
	}

	private class MyPanel extends JPanel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			for (int i = 0; i < boardSize; i++) {
				for (int j = 0; j < boardSize; j++) {
					g.drawRect(i*40 + 100, j*40 + 100, 40, 40);
				}
			}
			
			for (Triplet element : snake.getHead()) {
				g.setColor(Color.BLACK);
				g.fillRect(105 + element.getInfo()[0] * 40, 105 + element.getInfo()[1]*40, 30, 30);
			}
			
			g.setColor(Color.BLUE);
			g.fillRect(105 + foodLoc[0]*40, 105 + foodLoc[1]*40, 30, 30);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_LEFT) {
			snake.setHeadDir(3);
		} else if (code == KeyEvent.VK_UP) {
			snake.setHeadDir(0);
		} else if (code == KeyEvent.VK_RIGHT) {
			snake.setHeadDir(1);
		} else if (code == KeyEvent.VK_DOWN) {
			snake.setHeadDir(2);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// No Action on KeyRelease
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// No Action on KeyEvent
		
	}

}
