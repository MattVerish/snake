package game;

import java.util.LinkedList;
import java.util.Random;

public class Snake {

	private LinkedList<Triplet> head;
	private Triplet nextBody;
	
	public Snake(int boardSize) {
		LinkedList<Triplet> head = new LinkedList<Triplet>();
		head.add(new Triplet(4, 4, 1));
		this.head = head;
	}
	
	public LinkedList<Triplet> getHead() {
		return this.head;
	}
	
	public void updateSnake() {
		nextBody = new Triplet(head.getLast());
		int dir = head.getFirst().getDir();
		for (Triplet element : head) {
			int newDir = element.getDir();
			element.updatePosition(dir);
			dir = newDir;
		}
	}
	
	public boolean checkCollision() {
		Triplet[] snakeArray = head.toArray(new Triplet[0]);
		Triplet headInfo = head.getFirst();
		for (int i = 1; i < snakeArray.length; i++) {
			if (headInfo.isLocEqual(snakeArray[i])) {
				return true;
			}
		}
		
		int[] info = headInfo.getInfo();
		if (info[0] < 0 || info[0] >= 10 || info[1] < 0 || info[1] >= 10) {
			return true;
		}
		return false;
	}
	
	public void growSnake() {
		head.add(nextBody);
	}
	
	public boolean isOccupied(int X, int Y) {
		for (Triplet elem : head) {
			int[] info = elem.getInfo();
			if (info[0] == X && info[1] == Y) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isOutOfBounds(int size) {
		int[] snakeHead = head.getFirst().getInfo();
		if (snakeHead[0] >= size || snakeHead[0] < 0 || snakeHead[1] >= size || snakeHead[1] < 0) {
			return true;
		}
		return false;
	}
	
	public int getHeadDir() {
		return head.getFirst().getDir();
	}
	
	public int[] getHeadInfo() {
		return head.getFirst().getInfo();
	}
	
	public void setHeadDir(int newDir) {
		if (Math.abs(head.getFirst().getDir() - newDir) != 2) {
			head.getFirst().setDir(newDir);
		}
	}

}
